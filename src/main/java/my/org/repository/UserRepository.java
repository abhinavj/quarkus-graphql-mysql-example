package my.org.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import my.org.model.User;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class UserRepository implements PanacheRepository<User> {

}
