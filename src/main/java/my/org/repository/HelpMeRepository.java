package my.org.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import my.org.model.TopicData;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class HelpMeRepository implements PanacheRepository<TopicData> {

}
