package my.org.model;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;


@Entity
public class TopicData {

    @Id @GeneratedValue
    long dataId;

    @Valid
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "techId")
    TechTopic techTopic;

    @NotNull(message = "specify solution location(true/false)")
    boolean remote;

    String remotePath;
    String localPath;
    String version;

    @NotNull(message = "describe issue")
    String issue;

    String solution;

    @NotNull(message = "specify solution location")
    String fileType;

    public TopicData() {
    }

    public TopicData(TechTopic techTopic, boolean remote, String remotePath, String localPath,
                     String version, String issue, String solution, String fileType) {
        this.techTopic = techTopic;
        this.remote = remote;
        this.remotePath = remotePath;
        this.localPath = localPath;
        this.version = version;
        this.issue = issue;
        this.solution = solution;
        this.fileType = fileType;
    }

    public long getDataId() {
        return dataId;
    }

    public TechTopic getTechTopic() {
        return techTopic;
    }

    public void setTechTopic(TechTopic techTopic) {
        this.techTopic = techTopic;
    }

    public boolean isRemote() {
        return remote;
    }

    public void setRemote(boolean remote) {
        this.remote = remote;
    }

    public String getRemotePath() {
        return remotePath;
    }

    public void setRemotePath(String remotePath) {
        this.remotePath = remotePath;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
