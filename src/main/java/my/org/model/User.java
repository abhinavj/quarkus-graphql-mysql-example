package my.org.model;


import org.eclipse.microprofile.graphql.Ignore;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.*;

@Entity
public class User {

    @Id @GeneratedValue
    long uid;

    @NotBlank(message = "specify user Name")
    @Size(min = 3, max = 24,message ="Name length should be less than 24 or greater than 3 words")
    String userName;

    @Email(message = "specify valid Email")
    String emailId;

    String role = "guest";

    @NotBlank(message = "specify password")
    @Size(min = 3, max = 12,message = "password should less than 12 or greater than 8")
    String password;

    public User(String userName, String emailId, String password) {
        this.userName = userName;
        this.emailId = emailId;
        this.role = "guest";
        this.password = password;
    }

    public User() {
    }

    public long getUid() {
        return uid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getRole() {
        return role;
    }

    @Ignore
    public void setRole(String role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
