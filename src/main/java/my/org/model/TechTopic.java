package my.org.model;

import org.eclipse.microprofile.graphql.Ignore;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;


@Entity
public class TechTopic {

    @Id @GeneratedValue
    public long techId;

    @NotBlank(message = "specify techName")
    public String techName;

    @NotBlank(message = "specify techType")
    public String techType;

    public String description;

    public TechTopic() {
    }

    public TechTopic(String techName, String techType, String description) {
        this.techName = techName;
        this.techType = techType;
        this.description = description;
    }

    public long getTechId() {
        return techId;
    }

    @Ignore
    public void setTechId(long techId) {
        this.techId = techId;
    }

    public String getTechName() {
        return techName;
    }

    public void setTechName(String techName) {
        this.techName = techName;
    }

    public String getTechType() {
        return techType;
    }

    public void setTechType(String techType) {
        this.techType = techType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
