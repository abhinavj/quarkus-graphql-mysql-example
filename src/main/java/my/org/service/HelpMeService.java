package my.org.service;

import my.org.exceptions.ServiceException;
import my.org.model.TopicData;

import java.util.List;

public interface HelpMeService extends CRUDService<TopicData> {

}
