package my.org.service;

import my.org.exceptions.ServiceException;
import my.org.model.TopicData;

import java.util.List;

public interface CRUDService<T> {


    List<T> findAllEntity();

    T findById(long id);

    T addEntity(T t) throws ServiceException;

    T deleteEntity(long id) throws ServiceException;
}
