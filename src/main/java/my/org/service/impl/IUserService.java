package my.org.service.impl;

import my.org.exceptions.ServiceException;
import my.org.model.User;
import my.org.repository.UserRepository;
import my.org.service.UserService;
import my.org.validation.HelpMeValidation;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class IUserService implements UserService {

    @Inject
    UserRepository userRepository;

    @Inject
    HelpMeValidation<User> helpMeValidation;

    public List<User> findAllEntity() {
        return userRepository.listAll();
    }

    public User findById(long userID) {
        return userRepository.findById(userID);
    }

    @Transactional
    public User addEntity(User user) throws ServiceException {
        helpMeValidation.validateData(user);
        userRepository.persist(user);
        return user;
    }

    @Transactional
    public User deleteEntity(long userID) throws ServiceException {
        User user = findById(userID);
        if(user == null)
            throw new ServiceException("user not found");
        userRepository.delete(user);
        return user;
    }

    @Transactional
    public User addRole(long userID,String role) throws ServiceException {
        User user = findById(userID);
        user.setRole(role);
        return user;
    }
}
