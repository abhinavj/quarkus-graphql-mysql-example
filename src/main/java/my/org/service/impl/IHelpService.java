package my.org.service.impl;


import my.org.exceptions.ServiceException;
import my.org.model.TopicData;
import my.org.repository.HelpMeRepository;
import my.org.service.HelpMeService;
import my.org.validation.HelpMeValidation;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;


@ApplicationScoped
public class IHelpService implements HelpMeService {

    @Inject
    HelpMeRepository helpMeRepository;

    @Inject
    HelpMeValidation<TopicData> helpMeValidation;

    public IHelpService() {}

    public List<TopicData> findAllEntity() {
        return helpMeRepository.listAll();
    }

    public TopicData findById(long topicId) {
        return helpMeRepository.findById(topicId);
    }

    @Transactional
    public TopicData addEntity(TopicData topicData) throws ServiceException {
        helpMeValidation.validateData(topicData);

        helpMeRepository.persist(topicData);

        return topicData;
    }

    @Transactional
    public TopicData deleteEntity(long topicId) {
        TopicData topicData = findById(topicId);
        helpMeRepository.deleteById(topicId);
        return topicData;
    }


}