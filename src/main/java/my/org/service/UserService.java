package my.org.service;


import my.org.exceptions.ServiceException;
import my.org.model.User;

import java.util.List;

public interface UserService extends CRUDService<User>{

    User addRole(long userID,String role) throws ServiceException;

}
