package my.org.exceptions;

import org.eclipse.microprofile.graphql.GraphQLException;

public class ServiceException extends GraphQLException {

    public ServiceException(String message){
        super(message);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(GraphQLException.ExceptionType type) {
        super(type);
    }

    public ServiceException(String message, GraphQLException.ExceptionType type) {
        super(message,type);
    }

    public ServiceException(Throwable cause, GraphQLException.ExceptionType type) {
        super(cause,type);
    }

    public ServiceException(String message, Throwable cause, GraphQLException.ExceptionType type) {
        super(message, cause,type);
    }

    public ServiceException(Object partialResults) {
        super(partialResults);
    }

    public ServiceException(String message, Object partialResults) {
        super(message,partialResults);
    }

    public ServiceException(Throwable cause, Object partialResults) {
        super(cause,partialResults);
    }

    public ServiceException(String message, Throwable cause, Object partialResults) {
        super(message, cause,partialResults);
    }

}
