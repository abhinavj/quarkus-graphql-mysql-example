package my.org;


import my.org.exceptions.ServiceException;
import my.org.model.User;
import my.org.service.UserService;
import org.eclipse.microprofile.graphql.Description;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Mutation;
import org.eclipse.microprofile.graphql.Query;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import java.util.List;

@GraphQLApi
public class UserGraphqlAPIHandler {

    @Inject
    UserService service;

    @Query("allUser")
    @Description("Get all user")
    public List<User> getAllName() {
        return service.findAllEntity();
    }

    @Query("findUserById")
    public User getById(long id){
        return service.findById(id);
    }

    @Mutation("addUser")
    public User addTopicDB(User user) throws ServiceException {
        return service.addEntity(user);
    }

    @Mutation("deleteUser")
    public User deleteTopicDB(long id) throws ServiceException {
        return service.deleteEntity(id);
    }

    @RolesAllowed("admin")
    @Mutation("addRole")
    public User addRole(long id,String role) throws ServiceException {
        return service.addRole(id,role);
    }

}
