package my.org.validation.impl;

import my.org.exceptions.ServiceException;
import my.org.model.TechTopic;
import my.org.model.TopicData;
import my.org.validation.HelpMeValidation;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;
import java.util.stream.Collectors;


@ApplicationScoped
public class IHelpMeValidator<T> implements HelpMeValidation<T>{

    @Inject
    Validator validation;

    public void validateData(T data) throws ServiceException {
        Set<ConstraintViolation<Object>> constraintViolationSet = validation.validate(data);
        if(!constraintViolationSet.isEmpty()) {
            String message = constraintViolationSet.stream()
                    .map(ConstraintViolation::getMessage)
                    .collect(Collectors.joining(", "));
            throw new ServiceException(message);
        }
    }
}
