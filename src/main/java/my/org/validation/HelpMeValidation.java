package my.org.validation;

import my.org.exceptions.ServiceException;

public interface HelpMeValidation<T> {

    void validateData(T t) throws ServiceException;

}
