package my.org;

import my.org.exceptions.ServiceException;
import my.org.model.TopicData;
import my.org.service.HelpMeService;
import my.org.service.impl.IHelpService;
import org.eclipse.microprofile.graphql.*;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@GraphQLApi
public class GraphQLAPIHandler {

    @Inject
    HelpMeService service;

    @Query("allTopic")
    @Description("Get all Films from a galaxy far far away")
    public List<TopicData> getAllName() {
        return service.findAllEntity();
    }

    @Query("findTopicById")
    public TopicData getById(long id){
        return service.findById(id);
    }

    @Mutation("addTopic")
    public TopicData addTopicDB(TopicData topicData) throws ServiceException {
        return service.addEntity(topicData);
    }

    @Mutation("deleteTopic")
    public TopicData deleteTopicDB(long id) throws ServiceException {
        return service.deleteEntity(id);
    }

}
